<?php

declare(strict_types=1);

namespace App\Tests\Integration\Statistics;

use App\DTO\Overtime;
use App\Statistics\OvertimeRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class OvertimeRepositoryTest extends KernelTestCase
{
    private OvertimeRepository $repository;

    protected function setUp(): void
    {
        self::bootKernel();
        $repository = self::$container->get(OvertimeRepository::class);
        assert($repository instanceof OvertimeRepository);

        $this->repository = $repository;
    }

    public function testItCanFindByDate(): void
    {
        $found = $this->repository->byHotelAndDateRange(
            1,
            new DateTimeImmutable('2020-01-01'),
            new DateTimeImmutable('2020-01-15')
        );

        $found = iterator_to_array($found);
        $this->assertNotEmpty($found);

        /** @var Overtime $object */
        foreach ($found as $object) {
            $this->assertInstanceOf(Overtime::class, $object);
            $this->assertGreaterThanOrEqual(1, $object->getAverageScore());
            $this->assertLessThanOrEqual(5, $object->getAverageScore());
            $this->assertGreaterThanOrEqual(1, $object->getReviewCount());
            $this->assertRegExp('/^2020-01-[0-9]{2}$/', $object->getDateGroup());
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class OvertimeTest extends WebTestCase
{
    public function testRouteExists(): void
    {
        $client = self::createClient();
        $from = '2020-01-01';
        $to = '2020-01-15';
        $client->request('GET', "/overtime/1/{$from}...{$to}");

        $response = $client->getResponse();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetDailyOvertime(): void
    {
        $client = self::createClient();
        $from = '2020-01-01';
        $to = '2020-01-15';
        $client->request('GET', "/overtime/1/{$from}...{$to}");

        $response = $client->getResponse();

        $this->assertJson($response->getContent());
        $decoded = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('items', $decoded);
        $this->assertCount(14, $decoded['items']);

        foreach ($decoded['items'] as $item) {
            $this->assertArrayHasKey('review-count', $item);
            $this->assertArrayHasKey('average-score', $item);
            $this->assertArrayHasKey('date-group', $item);
        }
    }
}

<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Unit\Serializer\NameConverter;

use App\Serializer\NameConverter\CamelCaseToKebabCaseNameConverter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

/**
 * @author Kévin Dunglas <dunglas@gmail.com>
 * @author Davide Bellettini <davide@bellettini.eu>
 */
class CamelCaseToKebabCaseNameConverterTest extends TestCase
{
    public function testInterface()
    {
        $attributeMetadata = new CamelCaseToKebabCaseNameConverter();
        $this->assertInstanceOf(NameConverterInterface::class, $attributeMetadata);
    }

    /**
     * @dataProvider attributeProvider
     */
    public function testNormalize($dashed, $camelCased, $useLowerCamelCase)
    {
        $nameConverter = new CamelCaseToKebabCaseNameConverter(null, $useLowerCamelCase);
        $this->assertEquals($nameConverter->normalize($camelCased), $dashed);
    }

    /**
     * @dataProvider attributeProvider
     */
    public function testDenormalize($dashed, $camelCased, $useLowerCamelCase)
    {
        $nameConverter = new CamelCaseToKebabCaseNameConverter(null, $useLowerCamelCase);
        $this->assertEquals($nameConverter->denormalize($dashed), $camelCased);
    }

    public function attributeProvider()
    {
        return [
            ['coop-tilleuls', 'coopTilleuls', true],
            ['_kevin-dunglas', '_kevinDunglas', true],
            ['-kevin-dunglas', '-kevinDunglas', true],
            ['this-is-a-test', 'thisIsATest', true],
            ['coop-tilleuls', 'CoopTilleuls', false],
            ['_kevin-dunglas', '_kevinDunglas', false],
            ['this-is-a-test', 'ThisIsATest', false],
        ];
    }
}

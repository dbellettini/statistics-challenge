<?php

declare(strict_types=1);

namespace App\Tests\Unit\DateTime;

use App\DateTime\DateRange;
use App\DateTime\DateRangeIterator;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class DateRangeIteratorTest extends TestCase
{
    public function testSimpleAlignedDays(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-01-04');

        $range = new DateRange($from, $to);

        $expected = [
            new DateRange(
                new DateTimeImmutable('2020-01-01'),
                new DateTimeImmutable('2020-01-02'),
                '2020-01-01'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-02'),
                new DateTimeImmutable('2020-01-03'),
                '2020-01-02'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-03'),
                new DateTimeImmutable('2020-01-04'),
                '2020-01-03'
            ),
        ];

        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }

    public function testUnalignedDays(): void
    {
        $from = new DateTimeImmutable('2020-01-01T12:34:56.689');
        $to = new DateTimeImmutable('2020-01-04T23:45:12.123');

        $range = new DateRange($from, $to);

        $expected = [
            new DateRange(
                new DateTimeImmutable('2020-01-01T12:34:56.689'),
                new DateTimeImmutable('2020-01-02'),
                '2020-01-01'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-02'),
                new DateTimeImmutable('2020-01-03'),
                '2020-01-02'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-03'),
                new DateTimeImmutable('2020-01-04'),
                '2020-01-03'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-04'),
                new DateTimeImmutable('2020-01-04T23:45:12.123'),
                '2020-01-04'
            ),
        ];

        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }

    public function testUnalignedWeek(): void
    {
        $from = new DateTimeImmutable('2020-01-05');
        $to = new DateTimeImmutable('2020-02-11');

        $expected = [
            new DateRange(
                new DateTimeImmutable('2020-01-05'),
                new DateTimeImmutable('2020-01-06'),
                '2020W01'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-06'),
                new DateTimeImmutable('2020-01-13'),
                '2020W02'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-13'),
                new DateTimeImmutable('2020-01-20'),
                '2020W03'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-20'),
                new DateTimeImmutable('2020-01-27'),
                '2020W04'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-27'),
                new DateTimeImmutable('2020-02-03'),
                '2020W05'
            ),
            new DateRange(
                new DateTimeImmutable('2020-02-03'),
                new DateTimeImmutable('2020-02-10'),
                '2020W06'
            ),
            new DateRange(
                new DateTimeImmutable('2020-02-10'),
                new DateTimeImmutable('2020-02-11'),
                '2020W07'
            ),
        ];

        $range = new DateRange($from, $to);
        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }

    public function testExactly29Days(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-01-30');
        $range = new DateRange($from, $to);

        $this->assertEquals(29, iterator_count(DateRangeIterator::from($range)));
    }

    public function testExactly30Days(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-01-31');
        $range = new DateRange($from, $to);

        $this->assertEquals(5, iterator_count(DateRangeIterator::from($range)));
    }

    public function testAlignedWeek(): void
    {
        $from = new DateTimeImmutable('2020-01-06');
        $to = new DateTimeImmutable('2020-02-10');

        $expected = [
            new DateRange(
                new DateTimeImmutable('2020-01-06'),
                new DateTimeImmutable('2020-01-13'),
                '2020W02'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-13'),
                new DateTimeImmutable('2020-01-20'),
                '2020W03'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-20'),
                new DateTimeImmutable('2020-01-27'),
                '2020W04'
            ),
            new DateRange(
                new DateTimeImmutable('2020-01-27'),
                new DateTimeImmutable('2020-02-03'),
                '2020W05'
            ),
            new DateRange(
                new DateTimeImmutable('2020-02-03'),
                new DateTimeImmutable('2020-02-10'),
                '2020W06'
            ),
        ];

        $range = new DateRange($from, $to);
        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }

    public function testExactly89Days(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-03-30');
        $range = new DateRange($from, $to);

        $this->assertEquals(13, iterator_count(DateRangeIterator::from($range)));
    }

    public function testExactly90Days(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-03-31');
        $range = new DateRange($from, $to);

        $this->assertEquals(3, iterator_count(DateRangeIterator::from($range)));
    }

    public function testAlignedMonth(): void
    {
        $from = new DateTimeImmutable('2020-01-01');
        $to = new DateTimeImmutable('2020-05-01');

        $expected = [
            new DateRange(new DateTimeImmutable('2020-01-01'), new DateTimeImmutable('2020-02-01'), '2020-01'),
            new DateRange(new DateTimeImmutable('2020-02-01'), new DateTimeImmutable('2020-03-01'), '2020-02'),
            new DateRange(new DateTimeImmutable('2020-03-01'), new DateTimeImmutable('2020-04-01'), '2020-03'),
            new DateRange(new DateTimeImmutable('2020-04-01'), new DateTimeImmutable('2020-05-01'), '2020-04'),
        ];

        $range = new DateRange($from, $to);
        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }

    public function testUnalignedMonth(): void
    {
        $from = new DateTimeImmutable('2019-12-30T12:34:56.789');
        $to = new DateTimeImmutable('2020-05-21');

        $expected = [
            new DateRange(new DateTimeImmutable('2019-12-30T12:34:56.789'), new DateTimeImmutable('2020-01-01'), '2019-12'),
            new DateRange(new DateTimeImmutable('2020-01-01'), new DateTimeImmutable('2020-02-01'), '2020-01'),
            new DateRange(new DateTimeImmutable('2020-02-01'), new DateTimeImmutable('2020-03-01'), '2020-02'),
            new DateRange(new DateTimeImmutable('2020-03-01'), new DateTimeImmutable('2020-04-01'), '2020-03'),
            new DateRange(new DateTimeImmutable('2020-04-01'), new DateTimeImmutable('2020-05-01'), '2020-04'),
            new DateRange(new DateTimeImmutable('2020-05-01'), new DateTimeImmutable('2020-05-21'), '2020-05'),
        ];

        $range = new DateRange($from, $to);
        $this->assertEquals($expected, iterator_to_array(DateRangeIterator::from($range)));
    }
}

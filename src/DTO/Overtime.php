<?php

declare(strict_types=1);

namespace App\DTO;

class Overtime
{
    private int $reviewCount;

    private float $averageScore;

    private string $dateGroup;

    public function getReviewCount(): int
    {
        return $this->reviewCount;
    }

    public function setReviewCount(int $reviewCount): self
    {
        $this->reviewCount = $reviewCount;

        return $this;
    }

    public function getAverageScore(): float
    {
        return $this->averageScore;
    }

    public function setAverageScore(float $averageScore): self
    {
        $this->averageScore = $averageScore;

        return $this;
    }

    public function getDateGroup(): string
    {
        return $this->dateGroup;
    }

    public function setDateGroup(string $dateGroup): self
    {
        $this->dateGroup = $dateGroup;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\DateTime;

use DateTimeImmutable;

class DateRange
{
    private DateTimeImmutable $from;
    private DateTimeImmutable $to;
    private string $label;

    public function __construct(DateTimeImmutable $from, DateTimeImmutable $to, string $label = '')
    {
        $this->from = $from;
        $this->to = $to;
        $this->label = $label;
    }

    public function from(): DateTimeImmutable
    {
        return $this->from;
    }

    public function to(): DateTimeImmutable
    {
        return $this->to;
    }

    public function label(): string
    {
        return $this->label;
    }
}

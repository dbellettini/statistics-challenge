<?php

declare(strict_types=1);

namespace App\DateTime;

use DateInterval;
use DateTimeImmutable;
use Iterator;
use IteratorAggregate;

abstract class DateRangeIterator implements IteratorAggregate
{
    protected DateRange $range;

    private function __construct(DateRange $range)
    {
        $this->range = $range;
    }

    public static function from(DateRange $range): self
    {
        $from = $range->from();
        $to = $range->to();

        if ($to >= $from->add(new DateInterval('P90D'))) {
            return new MonthlyIterator($range);
        }

        if ($to >= $from->add(new DateInterval('P30D'))) {
            return new WeeklyIterator($range);
        }

        return new DailyIterator($range);
    }

    public function getIterator(): Iterator
    {
        $from = $this->range->from();
        $to = $this->range->to();

        $step = $this->step();

        for ($date = $this->alignStart($from); $date < $to; $date = $date->add($step)) {
            yield new DateRange(
                $this->limitStart($date),
                $this->limitEnd($date->add($step)),
                $this->formatDate($date)
            );
        }
    }

    abstract protected function alignStart(DateTimeImmutable $start): DateTimeImmutable;

    protected function limitStart(DateTimeImmutable $start): DateTimeImmutable
    {
        if ($start < $this->range->from()) {
            return $this->range->from();
        }

        return $start;
    }

    protected function limitEnd(DateTimeImmutable $end): DateTimeImmutable
    {
        if ($end > $this->range->to()) {
            return $this->range->to();
        }

        return $end;
    }

    abstract protected function step(): DateInterval;

    abstract protected function formatDate(DateTimeImmutable $date): string;
}

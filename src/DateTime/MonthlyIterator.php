<?php

declare(strict_types=1);

namespace App\DateTime;

use DateInterval;
use DateTimeImmutable;

class MonthlyIterator extends DateRangeIterator
{
    protected function step(): DateInterval
    {
        return new DateInterval('P1M');
    }

    protected function formatDate(DateTimeImmutable $date): string
    {
        return $date->format('Y-m');
    }

    protected function alignStart(DateTimeImmutable $start): DateTimeImmutable
    {
        return $start->modify('first day of this month midnight');
    }
}

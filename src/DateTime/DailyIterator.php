<?php

declare(strict_types=1);

namespace App\DateTime;

use DateInterval;
use DateTimeImmutable;

class DailyIterator extends DateRangeIterator
{
    protected function step(): DateInterval
    {
        return new DateInterval('P1D');
    }

    protected function formatDate(DateTimeImmutable $date): string
    {
        return $date->format('Y-m-d');
    }

    protected function alignStart(DateTimeImmutable $start): DateTimeImmutable
    {
        return $start->modify('midnight');
    }
}

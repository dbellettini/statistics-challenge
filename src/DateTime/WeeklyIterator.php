<?php

declare(strict_types=1);

namespace App\DateTime;

use DateInterval;
use DateTimeImmutable;

class WeeklyIterator extends DateRangeIterator
{
    protected function step(): DateInterval
    {
        return new DateInterval('P1W');
    }

    protected function formatDate(DateTimeImmutable $date): string
    {
        return $date->format('o\\WW');
    }

    protected function alignStart(DateTimeImmutable $start): DateTimeImmutable
    {
        return new DateTimeImmutable($start->format('o\\WW'), $start->getTimezone());
    }
}

<?php

declare(strict_types=1);

namespace App\Controller;

use App\Statistics\OvertimeRepository;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/overtime")
 * Class OvertimeController
 */
class OvertimeController extends AbstractController
{
    private OvertimeRepository $repository;
    private SerializerInterface $serializer;

    public function __construct(OvertimeRepository $repository, SerializerInterface $serializer)
    {
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/{id}/{dateFrom}...{dateTo}", name="overtime_default")
     */
    public function defaultAction(int $id, string $dateFrom, string $dateTo): Response
    {
        $items = $this->repository->byHotelAndDateRange(
            $id,
            new DateTimeImmutable($dateFrom),
            new DateTimeImmutable($dateTo)
        );
        $raw = $this->serializer->serialize(compact('items'), 'json');

        return new JsonResponse($raw, 200, [], true);
    }
}

<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Hotel;
use App\Entity\Review;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private const WORDS = [
        'alpha',
        'bravo',
        'charlie',
        'delta',
        'echo',
        'foxtrot',
        'golf',
        'hotel',
        'india',
        'juliett',
        'kilo',
        'lima',
        'mike',
        'november',
        'oscar',
        'papa',
        'quebec',
        'romeo',
        'sierra',
        'tango',
        'uniform',
        'victor',
        'whiskey',
        'x-ray',
        'yankee',
        'zulu',
    ];

    public function load(ObjectManager $manager)
    {
        $hotels = $this->generateHotels();
        foreach ($hotels as $hotel) {
            $manager->persist($hotel);
        }

        foreach ($this->generateReviews($hotels) as $review) {
            $manager->persist($review);
        }

        $manager->flush();
    }

    /**
     * @return Hotel[]
     */
    private function generateHotels(): array
    {
        $hotels = [];
        for ($i = 0; $i < 10; ++$i) {
            $hotel = new Hotel();
            $hotel->setName($this->generateRandomWords(3));

            $hotels[] = $hotel;
        }

        return $hotels;
    }

    /**
     * @param Hotel[] $hotels
     *
     * @return iterable<Review>
     */
    private function generateReviews(array $hotels): iterable
    {
        for ($i = 0; $i < 100000; ++$i) {
            $review = new Review();
            $review->setHotel($hotels[array_rand($hotels)]);
            $review->setComment($this->generateRandomWords(15));
            $review->setScore(random_int(1, 5));
            $review->setCreatedDate($this->randomDate());

            yield $review;
        }
    }

    private function generateRandomWords(int $length): string
    {
        $words = [];
        foreach (array_rand(self::WORDS, $length) as $key) {
            $words[] = self::WORDS[$key];
        }

        return implode(' ', $words);
    }

    private function randomDate(): DateTimeImmutable
    {
        $max = new DateTimeImmutable();
        $min = $max->modify('-2 years');

        return DateTimeImmutable::createFromFormat(
            'U',
            (string) random_int($min->getTimestamp(), $max->getTimestamp()),
            new DateTimeZone('UTC')
        );
    }
}

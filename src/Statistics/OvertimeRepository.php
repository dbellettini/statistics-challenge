<?php

declare(strict_types=1);

namespace App\Statistics;

use App\DateTime\DateRange;
use App\DateTime\DateRangeIterator;
use App\DTO\Overtime;
use App\Entity\Review;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;

class OvertimeRepository
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function byHotelAndDateRange(int $hotelId, DateTimeImmutable $from, DateTimeImmutable $to): iterable
    {
        $builder = $this->entityManager->createQueryBuilder();
        $builder
            ->select('COUNT(r) AS reviewCount, AVG(r.score) AS averageScore')
            ->from(Review::class, 'r')
            ->where('r.hotel = :hotelId')
            ->andWhere('r.createdDate >= :from')
            ->andWhere('r.createdDate < :to')
        ;

        $range = new DateRange($from, $to);

        foreach (DateRangeIterator::from($range) as $range) {
            $builder
                ->setParameter('hotelId', $hotelId)
                ->setParameter('from', $range->from())
                ->setParameter('to', $range->to())
            ;

            $query = $builder->getQuery();

            [$row] = $query->execute();

            yield (new Overtime())
                ->setAverageScore((float) $row['averageScore'])
                ->setReviewCount($row['reviewCount'])
                ->setDateGroup($range->label())
            ;
        }
    }
}

.PHONY: get-ready
get-ready: | reload composer-install-no-tty fixtures-sql migrations

.PHONY: build
build:
	docker-compose build

.PHONY: up
up:
	docker-compose up --build -d

.PHONY: down
down:
	docker-compose down --remove-orphans

.PHONY: reload
reload: | down up

.PHONY: bash
bash:
	docker-compose exec php bash

.PHONY: interactive
interactive:
	docker-compose exec php php -a

.PHONY: console
console:
	docker-compose exec php bin/console $(args)

.PHONY: composer
composer:
	docker-compose exec php composer $(args)

.PHONY: composer-install
composer-install:
	docker-compose exec php composer install

.PHONY: composer-install-no-tty
composer-install-no-tty:
	docker-compose exec -T php composer install --no-interaction

.PHONY: migrations
migrations:
	docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction

.PHONY: fixtures
fixtures:
	docker-compose exec php bin/console doctrine:fixtures:load --no-interaction

.PHONY: fixtures-sql
fixtures-sql:
	docker-compose exec -T db truncate-db
	docker-compose exec -T db load-fixture < src/DataFixtures/AppFixtures.sql

.PHONY: phpunit
phpunit:
	docker-compose exec php bin/phpunit $(args)

.PHONY: phpunit-no-tty
phpunit-no-tty:
	docker-compose exec -T php bin/phpunit $(args)

.PHONY: cs-fixer
cs-fixer:
	docker-compose exec php vendor/bin/php-cs-fixer fix -v

.PHONY: cs-fixer-dry
cs-fixer-dry:
	docker-compose exec php vendor/bin/php-cs-fixer fix -v --dry-run

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210507095909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add index to optimize overtime queries';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE INDEX overtime_idx ON review (hotel_id, created_date)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX overtime_idx ON review');
    }
}

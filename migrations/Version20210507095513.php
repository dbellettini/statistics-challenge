<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210507095513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Adds missing DC2Type comment';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE review CHANGE created_date created_date DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE review CHANGE created_date created_date DATETIME NOT NULL');
    }
}
